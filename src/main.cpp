#include <iomanip>
#include <iostream>
#include <libpq-fe.h>

void f_bdelimiter(int max, int nbcolumn, const char * bdelimiter){
	std::cout << std::endl;
        for(int nbdelimiter = 0; nbdelimiter < max * (nbcolumn+2); nbdelimiter++){
        	std::cout << bdelimiter;
        }
        std::cout << std::endl;
}

int main(){
	char *host;
        char *user;
        char *pass;
	char *bdd;
	char *port;
	PGresult *result;
	ExecStatusType result_status;
	int ssl;
	int protocol_version;
	int server_version;
	int client_version;
	PGPing code_retour_ping;
	code_retour_ping = PQping("host=postgresql.bts-malraux72.net port=5432");
	if(code_retour_ping == PQPING_OK){
		std::cout << "La connexion au serveur de base de données a été établie avec succès" << std::endl;

		PGconn *conn;
		conn=PQconnectdb("host=postgresql.bts-malraux72.net port=5432 dbname=h.fourmi user=h.fourmi connect_timeout=10");
		ConnStatusType status;
		status = PQstatus(conn);
		if (status == CONNECTION_OK){
			std::cout << "Connexion Valide" << std::endl;
	                ConnStatusType status;
               		status = PQstatus(conn);
			host = PQhost(conn);
                        user = PQuser(conn);
                        pass = PQpass(conn);
			bdd = PQdb(conn);
			port = PQport(conn);
			ssl = PQsslInUse(conn);
        		const char *encoding = PQparameterStatus(conn, "server_encoding");
			protocol_version = PQprotocolVersion(conn);
			server_version = PQserverVersion(conn);
			client_version = PQlibVersion();
			std::cout << "La connexion au serveur de base de données ’ "<< host <<" ’ a été établie avec les paramètres suivants : " << std::endl;
			std::cout << "utilisateur : " << user << std::endl;
			std::cout << "mot de passe : ";
			for(int i = 0; i < sizeof(pass); i++){
				std::cout << "*";
			}
			std::cout << std::endl;
			std::cout << "base de données : " << bdd <<std::endl;
			std::cout << "port TCP : " << port << std::endl;
			if (ssl == 1){
				std::cout << "chiffrement SSL : true" << std::endl;
			}else{
                              	std::cout << "chiffrement SSL : false" << std::endl;
			}
			std::cout << "encodage : " << encoding << std::endl;
			std::cout << "version du protocole : " << protocol_version << std::endl;
			std::cout << "version du serveur : " << server_version << std::endl;
			std::cout << "version de la bibliothèque ’ libpq ’ du client : " << client_version << std::endl;

			std::cout << std::endl;

    			result = PQexec(conn, "SELECT \"Animal\".id, \"Animal\".nom, \"Animal\".sexe, \"Animal\".date_naissance, \"Animal\".commentaires, \"Race\".nom, \"Race\".description FROM si6.\"Animal\" INNER JOIN \"Race\" ON \"Animal\".race_id = \"Race\".id WHERE sexe = 'Femelle' AND \"Race\".nom = 'Singapura'");
			result_status = PQresultStatus(result);
			if(result_status == PGRES_TUPLES_OK){
				int nbline = PQntuples(result);
				int nbcolumn = PQnfields(result);
				const char* delimiter = " | ";
				const char* bdelimiter = "-";

				int counter=0;
				int max = 0;
				for(int i = 0; i < nbcolumn; i++){
					char *namefield = PQfname(result,i);
					for(int l = 0; namefield[l] != '\0'; l++){
						counter++;
					}
					if(counter > max){
						max = counter;
					}
					counter = 0;

				}
				f_bdelimiter(max,nbcolumn,bdelimiter);
				for (int h = 0; h < nbcolumn; h++){
					char *header = PQfname(result,h);
					std::cout << delimiter << std::setw(max) << std::left << header;
				}
				std::cout << delimiter;
				f_bdelimiter(max,nbcolumn,bdelimiter);
				for(int l = 0; l < nbline; ++l){
					for(int c = 0; c < nbcolumn; ++c){
						char *fields_values = PQgetvalue(result,l,c);

						for(int field_size = 0; fields_values[field_size] != '\0'; field_size++){
							counter++;
						}
						if(counter > max){
							fields_values[max] = '\0';

							for(int field_size = max - 3; field_size < max; field_size++){
								fields_values[field_size] = '.';
							}
						}
						counter = 0;
						std::cout << delimiter << std::setw(max) << std::left << PQgetvalue(result,l,c);
					}
					std::cout << delimiter << std::endl;
				}
				f_bdelimiter(max,nbcolumn,bdelimiter);
				std::cout << std::endl;
				std::cout << "L'exécution de la requête SQL a retourné " << nbline << " enregistrements." << std::endl;
			}else if(result_status == PGRES_EMPTY_QUERY){
      				std::cout << "La chaîne envoyée au serveur était vide." << std::endl;
			}else if(result_status == PGRES_COMMAND_OK){
				std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée. " << std::endl;
			}else if(result_status == PGRES_COPY_OUT){
      				std::cout << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
			}else if(result_status == PGRES_COPY_IN){
      				std::cout << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
    			}else if(result_status == PGRES_BAD_RESPONSE){
      				std::cout << "La réponse du serveur n'a pas été comprise." << std::endl;
			}else if(result_status == PGRES_NONFATAL_ERROR){
      				std::cout << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
    			}else if(result_status == PGRES_FATAL_ERROR){
      				std::cout << "Une erreur fatale est survenue." << std::endl;
    			}else if(result_status == PGRES_COPY_BOTH){
      				std::cout << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur). Cette fonctionnalité est seulement utilisée par la réplication en flux, ce statut ne devrait donc pas apparaître dans les applications ordinaires." << std::endl;
   			}else if(result_status == PGRES_SINGLE_TUPLE){
      				std::cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante. Ce statut n'intervient que lorsque le mode ligne-à-ligne a été sélectionné pour cette requête" << std::endl;
    			}
		}else{
			std::cerr << "Connexion Invalide" << std::endl;
		}
	}else{
		std::cerr << "Malheureusement le serveur n’est pas joignable . Vérifier la connectivité" << std::endl;
	}
	return 0;
}
